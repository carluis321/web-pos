<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-semi-dark/login-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Jun 2017 01:40:22 GMT -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>Login</title>
  <link rel="apple-touch-icon" href="{{url('/')}}/app-assets/images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/feather/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/icheck/custom.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN STACK CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap-extended.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/colors.min.css">
  <!-- END STACK CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/pages/login-register.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/style.css">
  <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-full-screen-image blank-page blank-page">
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-3 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
              <div class="card-header no-border">
                <div class="card-title text-xs-center">
                  <h3>
                    <img src="{{url('/')}}/app-assets/images/logo/stack-logo.png" alt="branding logo">
                    Web Pos
                  </h3>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <form class="form-horizontal" action="{{ url('/sign-up') }}" novalidate method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" class="form-control" id="user-name" placeholder="Usuario"
                      required name="username">
                      <div class="form-control-position">
                        <i class="ft-user"></i>
                      </div>
                    </fieldset>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="password" class="form-control" id="user-password" placeholder="Password"
                      required name="password">
                      <div class="form-control-position">
                        <i class="fa fa-key"></i>
                      </div>
                    </fieldset>
                    <fieldset class="form-group">
                      @include('messages')
                    </fieldset>
                    <button type="submit" class="btn btn-outline-primary btn-block">
                      <i class="ft-unlock"></i>
                      Login
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <!-- BEGIN VENDOR JS-->
  <script src="{{url('/')}}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{url('/')}}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"
  type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{url('/')}}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- END PAGE LEVEL JS-->
</body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-semi-dark/login-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Jun 2017 01:40:22 GMT -->
</html>