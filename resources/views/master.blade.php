<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-semi-dark/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Jun 2017 01:38:42 GMT -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>Web Pos</title>
  <link rel="apple-touch-icon" href="{{url('/')}}/app-assets/images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/feather/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/unslider.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/weather-icons/climacons.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/meteocons/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/charts/morris.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN STACK CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap-extended.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/colors.min.css">
  <!-- END STACK CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/simple-line-icons/style.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/colors/palette-gradient.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/pages/timeline.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/style.css">
  <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <!-- navbar-fixed-top-->
  <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav">
          <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a href="{{ url('/') }}" class="navbar-brand">
              <img alt="stack admin logo" src="{{url('/')}}/app-assets/images/logo/stack-logo-light.png"
              class="brand-logo">
              <h2 class="brand-text">Web Pos</h2>
            </a>
          </li>
          <li class="nav-item hidden-md-up float-xs-right">
            <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content container-fluid">
        <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
          <ul class="nav navbar-nav">
            <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a>
            </li>
            {{-- <li class="nav-item nav-search"><a href="#" class="nav-link nav-link-search"><i class="ficon ft-search"></i></a>
              <div class="search-input">
                <input type="text" placeholder="Explore Stack..." class="input">
              </div>
            </li> --}}
          </ul>
          <ul class="nav navbar-nav float-xs-right">
            <li class="dropdown dropdown-notification nav-item">
              <a href="#" data-toggle="dropdown" class="nav-link nav-link-label" aria-expanded="true">
                <i class="ficon ft-bell"></i>
                @if(count(auth()->user()->unreadNotifications))
                  <span class="tag tag-pill tag-default tag-danger tag-default tag-up count-notification">
                    {{ count(auth()->user()->unreadNotifications) }}
                  </span>
                @endif
              </a>
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                    <span class="grey darken-2">Notifications</span>
                    <span class="notification-tag tag tag-default tag-danger float-xs-right m-0">
                      <span class="count-notification">
                        {{ count(auth()->user()->unreadNotifications) }}
                      </span> New
                    </span>
                  </h6>
                </li>
                <li class="list-group scrollable-container">
                  @foreach(auth()->user()->unreadNotifications as $notification)
                    <a href="#" class="list-group-item checkNotification" data-notify="{{$notification->id}}"
                      data-url="{{ url('/notifications/mark-as-read/' . $notification->id) }}">
                      <div class="media">
                        <div class="media-left valign-middle">
                          <i class="ft-plus-square icon-bg-circle bg-cyan"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            ¡Tienes una nueva compra de parte de {{$notification->data['client']}}
                          </h6>
                          <p class="notification-text font-small-3 text-muted">
                            {{ $notification->data['client'] }} ha comprado un total de <br />
                            $ {{ number_format($notification->data['total'], 2, ',', '.') }}
                          </p>
                          {{-- <small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted"></time>
                          </small> --}}
                        </div>
                      </div>
                    </a>
                  @endforeach
                </li>
                <li class="dropdown-menu-footer">
                  <a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">
                    Read all notifications
                  </a>
                </li>
              </ul>
            </li>
            <li class="dropdown dropdown-user nav-item">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                <span class="avatar avatar-online">
                  <img src="{{url('/')}}/img/pos-image.png" alt="avatar">
                  <i></i>
                </span>
                <span class="user-name">{{ auth()->user()->name }}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="ft-user"></i> Account</a>
                <div class="dropdown-divider"></div>
                <a href="{{ url('/logout') }}" class="dropdown-item">
                  <i class="ft-power"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-content">
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li class=" navigation-header">
          <span>General</span><i data-toggle="tooltip" data-placement="right" data-original-title="General"
          class=" ft-minus"></i>
        </li>
        <li class="nav-item">
          <a href="{{ url('/') }}">
            <i class="ft-home"></i>
            <span data-i18n="" class="menu-title">
              Dashboard
            </span>
          </a>
          {{-- <ul class="menu-content">
            <li class="active"><a href="dashboard-ecommerce.html" class="menu-item">eCommerce</a>
            </li>
            <li><a href="dashboard-analytics.html" class="menu-item">Analytics</a>
            </li>
            <li><a href="dashboard-fitness.html" class="menu-item">Fitness</a>
            </li>
          </ul> --}}
        </li>
        @if(auth()->user()->rol == 0)
          <li class="nav-item">
            <a href="{{ url('/companies') }}">Empresas</a>
          </li>
        @endif
        {{-- <li class="nav-item">
          <a href="{{ url('/clients') }}">Clientes</a>
        </li> --}}
        <li class="nav-item">
          <a href="{{ url('/orders') }}">
            Órdenes de Compra
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      @yield('ct')
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-xs-block d-md-inline-block">
        Copyright &copy; 2017
        <a href="#" target="_blank" class="text-bold-800 grey darken-2">
          US
        </a>,
        All rights reserved.
      </span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{url('/')}}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{url('/')}}/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{url('/')}}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/js/core/app.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{url('/')}}/app-assets/js/main.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-semi-dark/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Jun 2017 01:38:49 GMT -->
</html>