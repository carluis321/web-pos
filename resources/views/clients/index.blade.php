@extends('master')

@section('ct')
	<div class="content-body">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Clientes
					<a href="{{ url('/clients/create') }}" class="btn btn-success pull-right">
						Nuevo Cliente
					</a>
				</h2>
			</div>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Listado</h4>
					</div>
					<div class="card-body">
						<div class="card-block">
							@include('messages')
						</div>
						<div class="card-block">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<td>
												ID
											</td>
											<td>
												Nombre
											</td>
											<td>
												Cliente de
											</td>
											<td>
												Acciones
											</td>
										</tr>
									</thead>
									<tbody>
										@foreach($clients as $client)
											<td>
												{{ $client->user->username }}
											</td>
											<td>
												{{ $client->name }}
											</td>
											<td>
												{{ $client->company->name }}
											</td>
											<td>
												
											</td>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop