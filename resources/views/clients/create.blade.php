@extends('master')

@section('ct')
	<div class="content-body">
		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Agregar nuevo Cliente</h4>
					</div>
					<form action="{{ url('/clients') }}" method="POST" autocomplete="off">
						<div class="card-body">
							{{ csrf_field() }}
							<div class="card-block">
								<label>ID</label>
								<input type="text" name="id" class="form-control" />
							</div>
							<div class="card-block">
								<label>Nombre</label>
								<input type="text" name="name" class="form-control" />
							</div>
							<div class="card-block">
								<label>Clave</label>
								<input type="password" name="password" class="form-control" />
							</div>

							@if(auth()->user()->rol == 0)
								<div class="card-block">
									<label>Empresa</label>
									<select name="company_id" class="form-control">
										@foreach($companies as $company)
											<option value="{{$company->id}}">
												{{ $company->name }}
											</option>
										@endforeach
									</select>
								</div>
							@else

							@endif

							<div class="card-block">
								@include('messages')
							</div>
						</div>
						<div class="card-footer">
							<input type="submit" value="Agregar" class="btn btn-primary" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop