@extends('master')

@section('ct')

	<div class="content-body">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Empresas
					<a href="{{ url('/companies/create') }}" class="btn btn-success pull-right">
						Nueva Empresa
					</a>
				</h2>
			</div>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Listado</h4>
					</div>
					<div class="card-body">
						<div class="card-block">
							@include('messages')
						</div>
						<div class="card-block">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<td>ID</td>
											<td>Nombre</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody>
										@foreach($companies as $company)
											<tr>
												<td>
													{{ $company->user->username }}
												</td>
												<td>
													{{ $company->name }}
												</td>
												<td>
													<div class="btn-group">
														<a href="{{ url('/companies', $company->id) }}" class="btn btn-secondary btn-sm">
															<i class="fa fa-edit"></i>
														</a>
													</div>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop