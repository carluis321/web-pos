@extends('master')

@section('ct')

	<div class="content-body">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="card">
					<div class="card-header bg-cyan">
						<h2 class="text-md-center" style="color: white;">
							Orden de compra #{{ $order->id }}
						</h2>
					</div>
					<div class="card-body">
						<div class="card-block">
							<h4 class="text-md-center">
								Detalle de compra
							</h4>
						</div>
						<div class="row">
							@foreach($order->products as $product)
								<div class="col-md-6">
									<div class="card-block">
										<h5>
											{{ $product->name }}
										</h5>

										<h5>
											Precio: ${{ number_format($product->price, 0, '.', ',') }}
										</h5>

										<h5>
											Cantidad: {{ $product->quantity }}
										</h5>

										<h5>
											Sub Total: ${{ number_format(($product->price * $product->quantity), 0, '.', ',') }}
										</h5>
									</div>
								</div>
								@php
									$total += ($product->price * $product->quantity);
								@endphp
							@endforeach
						</div>
					</div>
					<div class="card-footer">
						<h4>
							Total: ${{ number_format($total, 0, '.', ',') }}
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop