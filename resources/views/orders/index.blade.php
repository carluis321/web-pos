@extends('master')

@section('ct')
	<div class="content-body">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Órdenes de Compra
				</h2>
			</div>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Listado</h4>
					</div>
					<div class="card-body">
						<div class="card-block">
							@include('messages')
						</div>
						<div class="card-block">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												
											</th>
											<th>
												ID
											</th>
											<th>
												Hecha por
											</th>
											<th>
												Empresa
											</th>
											<th>
												Fecha
											</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@foreach($orders as $order)
											@php $order->setClient() @endphp

											<tr>
												<td>
													<a href="{{ url('/orders/' . $order->id) }}" class="btn btn-info btn-sm">
														Ver
													</a>
												</td>
												<td>
													{{ $order->id }}
												</td>
												<td>
													{{ $order->client->name }}
												</td>
												<td>
													{{ $order->company->name }}
												</td>
												<td>
													{{ $order->created_at }}
												</td>
												<td>
													<a href="{{ url('/orders/export/' . $order->id) }}" class="btn btn-sm btn-default">
														<i class="fa fa-download"></i>
													</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop