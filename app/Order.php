<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'company_id',
    	'total',
    	'client',
    	'observation',
    ];

	public function company()
	{
		return $this->belongsTo('App\Company', 'company_id');
	}

	public function products()
	{
		return $this->hasMany('App\OrderDetail');
	}

	public function setClient()
	{
		$this->client = json_decode($this->client);
	}
}
