<?php
namespace App\Traits;

use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;

trait GoogleTrait {
	function initializeGoogle()
	{
		$client = new Google_Client();

		$client->setAuthConfig(base_path() . '/' . env('GOOGLE_SECRET'));

		$client->setApplicationName(env('GOOGLE_APP_NAME'));

		$client->setScopes([
			Google_Service_Drive::DRIVE
		]);

		$client->setAccessType("offline");

		$credentialsPath = base_path() . '/' . env('GOOGLE_CREDENTIALS');

		if (file_exists($credentialsPath)) {
			$accessToken = json_decode(file_get_contents($credentialsPath), true);
		}

		$client->setAccessToken($accessToken);

		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
		}

		$service = new Google_Service_Drive($client);

		return $service;
	}
}