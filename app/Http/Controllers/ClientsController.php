<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ClientsRepository;
use App\Repositories\CompaniesRepository;

use Google_Client;
use Google_Service_Drive;
use XBase\Table;

class ClientsController extends Controller
{

	protected $clients;
	protected $companies;

	public function __construct(ClientsRepository $clients, CompaniesRepository $companies)
	{
		$this->clients = $clients;
		$this->companies = $companies;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    return view('clients.index', [
	    	'clients' => $this->clients->all()
	    ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	    return view('clients.create', [
	    	'companies' => $this->companies->all()
	    ]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    $this->validate($request, [
	    	'id' => 'required',
	    	'name' => 'required|min:3',
	    	'password' => 'nullable|min:3',
	    	'company_id' => 'required',
	    ]);

	    $data = $request->all();

		array_forget($data, '_token');

		$data = [
			'name' => $data['name'],
			'username' => $data['id'],
			'password' => $data['password'],
			'company_id' => $data['company_id'],
		];

		$client = $this->clients->create($data);

		$request->session()->flash('msg', [
			'type' => 'success',
			'messages' => [
				'Se ha agregado el cliente: ' . $client->name . ' correctamente'
			]
		]);

		return redirect('/clients');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	    //
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	    //
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    //
	}

	public function getProducts(Request $request)
	{
		$drive = $this->initializeGoogle();

		// $parameters['q'] = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
		// $folderList = $drive->files->listFiles($parameters);
		// dd($folderList);

		$company = $this->companies->find($request->id);

		$optParams = array(
			'q' => "'$company->folder_id' in parents",
			'fields' => 'files(id, name)'
		);

		$results = $drive->files->listFiles($optParams);

		$productFileId = null;

		foreach ($results->getFiles() as $file) {
			if ($file->name == 'inventario.dbf') {
				$productFileId = $file->id;
				break;
			}
		}

		$response = $drive->files->get($productFileId, [
			'alt' => 'media'
		]);

		$content = $response->getBody();

		$name = $this->clients->find($request->id);

		$name = "$name->id-" . md5($name->name);

		file_put_contents(public_path() . '/dbf/' . $name . '.dbf', $content);

		$tableProducts = new Table(public_path() . '/dbf/' . $name . '.dbf');

		$products = [];

		while ($record = $tableProducts->nextRecord()) {
			$products[] = [
				'code' => $record->codigo,
				'article' => utf8_encode($record->articulo),
				'quantity' => $record->cant,
				'price' => number_format($record->valunit, 2, '.', ','),
				'rawPrice' => $record->valunit,
				'valunit1' => $record->valunit1,
			];
		}

		unlink(public_path() . '/dbf/' . $name . '.dbf');

		return response()->json($products);
	}
}
