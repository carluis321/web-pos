<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CompaniesRepository;
use App\Repositories\ClientsRepository;

use Auth;

use XBase\Table;

class HomeController extends Controller
{
	protected $companies;
	protected $clients;

	public function __construct(CompaniesRepository $companies, ClientsRepository $clients)
	{
		$this->companies = $companies;
		$this->clients = $clients;
	}

	public function index()
	{
		return view('index');
	}

	public function sLogin()
	{
		return view('login');
	}

	public function signUp(Request $request)
	{
		$this->validate($request, [
			'username' => 'required|min:3',
			'password' => 'required|min:3'
		]);

		$data = [
			'username' => $request->username,
			'password' => $request->password,
		];

		if (Auth::attempt($data)) {
			return redirect('/');
		}

		$request->session()->flash('msg', [
			'type' => 'warning',
			'messages' => [
				'Usuario o contraseña incorrectos'
			]
		]);

		return back();
	}

	public function logout()
	{
	    session()->flush();

	    return redirect('/login');
	}

	public function apiLogin(Request $request)
	{
	    $this->validate($request, [
	        'client' => 'required',
	        'company' => 'required',
	    ]);

	    $company = $this->companies->getByCode($request->company);

	    if (!$company) {
	    	return response()->json([
	    		'message' => 'Empresa no existe',
	    		'type' => 'warning'
	    	]);
	    }

	    $drive = $this->initializeGoogle();

	    $files = $drive->files->listFiles([
	    	'q' => "'$company->folder_id' in parents"
	    ]);

	    $clientes = null;

		foreach ($files->files as $file) {
			if ($file->name == 'clientes.dbf') {
				$clientes = $drive->files->get($file->id, [
					'alt' => 'media'
				]);

				$clientes = $clientes->getBody();
				break;
			}
		}

	   	$dbfFile = public_path() . '/dbf/' . str_random(40) . '.dbf';

	   	file_put_contents($dbfFile, $clientes);

	   	$clientes = new Table($dbfFile);

		while ($record = $clientes->nextRecord()) {
			// echo "<pre>";
			// 	var_dump($record);
			// echo "</pre>";


			if ($record->cc_nit == $request->client) {

				unlink($dbfFile);

				return response()->json([
					'message' => 'Has iniciado sesión correctamente',
					'client' => [
						'id' => $record->cc_nit,
						'name' => $record->nombres,
						'email' => $record->email,
						'phone' => $record->telefono,
						'address' => $record->domicilio,
					],
					'company' => $company->id
				]);
			}
		}

		unlink($dbfFile);

	    return response()->json([
			'message' => 'Cliente no concuerda con la empresa asignada',
			'type' => 'warning'
		]);

		// return response()->json([
		// 	'message' => 'Has iniciado sesión correctamente',
		// 	'client' => $client,
		// ]);
	}

	public function markAsRead($id)
	{
		auth()->user()->unreadNotifications->find($id)->markAsRead();

		return response()->json([
			'response' => 'success'
		]);
	}

	public function getClients($companyId){
		$company = $this->companies->getByCode($companyId);

	    if (!$company) {
	    	return response()->json([
	    		'message' => 'Empresa no existe',
	    		'type' => 'warning'
	    	]);
	    }

	    $drive = $this->initializeGoogle();

	    $files = $drive->files->listFiles([
	    	'q' => "'$company->folder_id' in parents"
	    ]);

	    $clientes = null;

		foreach ($files->files as $file) {
			if ($file->name == 'clientes.dbf') {
				$clientes = $drive->files->get($file->id, [
					'alt' => 'media'
				]);

				$clientes = $clientes->getBody();
				break;
			}
		}

		$dbfFile = public_path() . '/dbf/' . str_random(40) . '.dbf';

	   	file_put_contents($dbfFile, $clientes);

	   	$clientes = new Table($dbfFile);

	   	$clientsDBF = [];

		while ($record = $clientes->nextRecord()) {
			// echo "<pre>";
			// 	var_dump($record);
			// echo "</pre>";


			$clientsDBF[] = [
				'id' => e($record->cc_nit),
				'name' => e($record->nombres),
				'email' => e($record->email),
				'phone' => e($record->telefono),
				'address' => e($record->domicilio),
			];
		}

		unlink($dbfFile);

		return response()->json(['clients' => $clientsDBF]);
	}
}
