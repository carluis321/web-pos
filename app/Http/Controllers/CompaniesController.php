<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CompaniesRepository;

use Google_Service_Drive_DriveFile;

class CompaniesController extends Controller
{
	protected $companies;

	public function __construct(CompaniesRepository $companies)
	{
	    $this->companies = $companies;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('companies.index',[
			'companies' => $this->companies->all(),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('companies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// dd($request->all());
		$this->validate($request, [
			'name' => 'required|min:4',
			'id' => 'required',
			'password' => 'nullable|min:4'
		]);

		$data = $request->all();

		array_forget($data, '_token');

		$data = [
			'name' => $data['name'],
			'username' => $data['id'],
			'password' => $data['password'],
		];

		$company = $this->companies->create($data);

		$drive = $this->initializeGoogle();

		$name = str_replace(' ', '_', $company->name) . '_';

		$name = $name . sprintf('%04d',$company->id);

		$fileMetadata = new Google_Service_Drive_DriveFile([
			'name' => $name,
			'mimeType' => 'application/vnd.google-apps.folder'
		]);

		$companyFolder = $drive->files->create($fileMetadata, [
			'fields' => 'id, parents'
		]);

		$emptyFileMetadata = new Google_Service_Drive_DriveFile();

		$drive->files->update($companyFolder->id, $emptyFileMetadata, [
			'addParents' => '0B3DetovIYwQbVGJTYVJuRTlLelE',//folder empresas,
			'fields' => 'id, parents',
			'removeParents' => join(',', $companyFolder->parents)
		]);

		$this->companies->update($company->id, [
			'folder_id' => $companyFolder->id,
			'code' => sprintf('%04d',$company->id)
		]);

		$request->session()->flash('msg', [
			'type' => 'success',
			'messages' => [
				'Se ha agregado la empresa: ' . $company->name . ' correctamente'
			]
		]);

		return redirect('/companies');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	    //
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	    //
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    //
	}
}
