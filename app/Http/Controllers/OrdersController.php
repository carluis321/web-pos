<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\OrdersRepository;
use App\Repositories\UsersRepository;

use App\Notifications\OrderSend;

use XBase\WritableTable;
use XBase\Record;
use XBase\Table;

class OrdersController extends Controller
{
    protected $orders;
    protected $users;

    public function __construct(OrdersRepository $orders, UsersRepository $users)
    {
    	$this->orders = $orders;
    	$this->users = $users;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	switch (auth()->user()->rol) {
    		case 0:
    			$orders = $this->orders->all();
    		break;

    		case 1:
    			$orders = $this->orders->byCompany(auth()->user()->company->id);
    		break;
    	}
        return view('orders.index', [
        	'orders' => $orders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$total = 0;
		$products = [];

		foreach ($request->products as $order) {
			$products[] = $order;

		    $total += $order['price'];
		}

		$order = [
			'company_id' => $request->companyId,
			'total' => $total,
		    'observation' => $request->observation,
		    'client' => json_encode($request->client)
		];

		$order = $this->orders->create($order, $products);

		$user = $order->company->user;

		$admin = $this->users->find(1);

		$admin->notify(new OrderSend($order));

		$user->notify(new OrderSend($order));

		return response()->json(['response' => 'Orden de compra ID: ' . $order->id . ' enviada correctamente']);
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->orders->find($id);

        return view('orders.show', [
            'order' => $order,
            'total' => 0
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export($id)
    {
		$order = $this->orders->find($id);

		if (!$order) {
			return back();
		}

		$dbf_filename = rand(1000, 10000000000);

		$dbf_public_path = public_path() . '/dbf/';

		$dbf_path = $dbf_public_path.$dbf_filename.'.dbf';

		$dbf = \dbase_create($dbf_path, [
			[
				'cc_nit',
				'C',
				15
			],
			[
				'nombres',
				'C',
				60
			],
			[
				'domicilio',
				'C',
				60
			],
			[
				'telefono',
				'C',
				20
			],
			[
				'num_pedido',
				'C',
				15
			],
			[
				'codigo',
				'C',
				15
			],
			[
				'articulo',
				'C',
				60
			],
			[
				'cantidad',
				'C',
				15
			],
			[
				'valunit',
				'N',
				15,
				2
			],
			[
				'total',
				'C',
				15,
				2
			],
			[
				'obs',
				'C',
				150
			],
		]);

		$client = null;

		foreach ($order->products as $orderProduct) {
			$client = json_decode($order->client);

			\dbase_add_record($dbf, [
				$client->id, //cc_nit
				$client->name, //nombres
				$client->address, //domicilio
				$client->phone, //telefono
				$order->id, //num_pedido
				$orderProduct->product_id, //codigo
				$orderProduct->name, //articulo
				$orderProduct->quantity, //cantidad
				$orderProduct->price, //valunit
				($orderProduct->quantity * $orderProduct->price), //total
				$order->observation //obs
			]);
		}

		$record = null;

		$order->delete();

		return response()->download($dbf_path, "pedido-$order->id.dbf")->deleteFileAfterSend(true);
    }
}
