<?php

namespace App\Repositories;

use UsersRepository;

use App\Client;
use App\User;

class ClientsRepository {
	
	public function all()
	{
		return Client::all();
	}

	public function create($data)
	{
		$user = User::create($data);

		$data = array_add($data, 'user_id', $user->id);
		$data = array_add($data, 'company_id', $data['company_id']);
		// $data = array_add($data, 'company_id', 2);

		array_forget($data, 'username');
		array_forget($data, 'password');

		// dd($data);

		$client = Client::create($data);

		return $client;
	}

	public function find($id)
	{
		return User::find($id);
	}

	public function getByCompany($company, $idClient)
	{
		$client = User::where('username', $idClient)->first();

		if (!$client || $client->client->company_id != $company->id) {
			return false;
		}

		return $client;
	}
}
