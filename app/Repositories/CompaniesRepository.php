<?php

namespace App\Repositories;

use UsersRepository;

use App\Company;
use App\User;

use Hash;

class CompaniesRepository {
	
	public function all()
	{
		return Company::all();
	}

	public function create($data)
	{
		$data['password'] = Hash::make($data['password']);

		$user = User::create($data);

		$data = array_add($data, 'user_id', $user->id);

		array_forget($data, 'username');
		array_forget($data, 'password');

		$company = Company::create($data);

		return $company;
	}

	public function find($id)
	{
		return Company::find($id);
	}

	public function update($id, $data)
	{
		Company::where('id', $id)->update($data);
	}

	public function getByCode($code)
	{
		return Company::where('code', $code)->first();
	}
}
