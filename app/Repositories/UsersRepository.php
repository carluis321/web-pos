<?php

namespace App\Repositories;

use App\User;

class UsersRepository {
	
	public function all()
	{
		return User::all();
	}

	public function find($id)
	{
		return User::find($id);
	}
}
