<?php

namespace App\Repositories;

use App\Order;
use App\OrderDetail;

class OrdersRepository {
	
	public function all()
	{
		return Order::orderBy('id', 'DESC')->get();
	}

	public function find($id)
	{
		return Order::find($id);
	}

	public function create($order, $products)
	{
		$order = Order::create($order);

		foreach ($products as $product) {
			OrderDetail::create([
				'order_id' => $order->id,
				'product_id' => $product['code'],
				'name' => $product['product'],
				'price' => $product['price'],
				'quantity' => $product['quantity'],
			]);
		}

		return $order;
	}

	public function byCompany($id)
	{
		return Order::where('company_id', $id)->orderBy('id', 'DESC')->get();
	}
}
