<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $fillable = [
		'company_id',
		'user_id',
		'name',
	];

	public function company()
	{
		return $this->belongsTo('App\Company', 'company_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
