<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', ['uses' => 'HomeController@sLogin'])->name('login');

Route::post('/sign-up', ['uses' => 'HomeController@signUp']);

Route::group(['middleware' => ['auth', 'web']], function (){
	Route::get('/', ['uses' => 'HomeController@index']);

	Route::resource('/companies', 'CompaniesController');
	Route::resource('/clients', 'ClientsController');
	Route::resource('/orders', 'OrdersController');

	Route::get('/logout', ['uses' => 'HomeController@logout']);

	Route::get('/orders/export/{id}', ['uses' => 'OrdersController@export']);

	Route::group(['prefix' => '/notifications'], function (){
		Route::post('/mark-as-read/{id}', 'HomeController@markAsRead');
	});

	Route::get('/download/{file}', 'OrdersController@download');
});
